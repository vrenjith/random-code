#!/usr/bin/python
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-d", "--directory", dest="directory",
                  help="Directory containing the log file (recursively searched)")
parser.add_option("-z", "--zipfile", dest="zipfile",
                  help="Zip file containing the logs (usually support zip)")

parser.print_help();
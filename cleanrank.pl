#!/usr/bin/perl
use strict;

if (@ARGV eq 0) {
    print "\n";
    print "This script will remove ranking from a given head, this is useful if a rank chain is broken by only a little\n";
    print "Arguments: script.pl <activeobjects.xml> <ranking head>\n";
    print "\n";
    exit 1;
}

open FF, $ARGV[0];
my $head = $ARGV[1];

my @finaldata;

cleandata($head);

print("Starting with head [$head]\n");

sub cleandata
{
    my $head = shift;
    my $skip = 0;
    foreach(<FF>)
    {
        if($skip)
        {
            print("Skipping line - $_");
            $skip--;
            if(1 eq $skip)
            {
                print("Finding next head\n");
                $head = $1 if(m/<integer>(\d+)<\/integer>/);
                print("Next head [$head]\n");
            }
            if(!$skip)
            {
                print("Recursion with [$head]\n");
                push(@finaldata, <FF>);
                cleandata($head);
            }
            next;
        }
        if(m/<integer>$head<\/integer>/)
        {
            print("Block found with [$head]\n");
            #Get rid of three previous lines
            pop(@finaldata);
            pop(@finaldata);
            pop(@finaldata);
            $skip = 2;
        }
        else
        {
            push(@finaldata,$_);
        }
    }
}

close(FF);

print "Fixed file, putting into out.xml\n";
open OO,">out.xml";
print OO @finaldata;
close(OO);

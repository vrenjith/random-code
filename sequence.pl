use strict;

my @files = (
    'atlassian-jira.log.5', 'atlassian-jira.log.4',
    'atlassian-jira.log.3', 'atlassian-jira.log.2',
    'atlassian-jira.log.1', 'atlassian-jira.log'
);

my @exp = ("jira-greenhopper-plugin-(.*?)\.jar");

foreach (@files) {
    open FF, $_;
    foreach (<FF>) {
        my $line = $_;
        foreach (@exp) {
            if ( $line =~ m/(\d+-\d+-\d+ \d+:\d+:\d+).*?$_/ ) {
                print $line;
                print STDERR "$1 $2\n";
            }
        }
    }
    close(FF);
}
__END__
my @exp = (
    'Running JIRA startup checks',
    'JIRA Build\s+:',
    'Java Version\s:',
    'Maximum Allowable Memory\s:',
    'DBCP',
    'SQL.*?Exception'
);

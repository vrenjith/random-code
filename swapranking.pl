use strict;

my $jirainst = '/Users/rpillai/userdata/jira.5.2.1/jira';
my $rankingCount = 1;
my $loopCount    = 1;

restartJIRA();

my $pid = fork();

if ($pid) {
    while (1) {
        print ntime() . " Loop [$loopCount] Fetching issue list";
        $loopCount++;

`wget -q -O issuesgh.xml 'http://localhost:8521/sr/jira.issueviews:searchrequest-xml/10001/SearchRequest-10001.xml?tempMax=1000'`;
        if ($?) {
            print " - failure\n";
            next;
        }
        else {
            print " - success\n";
        }
        my @issues = ();
        open FF, "issuesgh.xml";
        foreach (<FF>) {
            if (m/<title>\[(TG-\d+)\]/) {
                push( @issues, $1 );
            }
        }
        while ( scalar(@issues) > 2 )    #At least three issues in the array
        {
            my $issueA = shift(@issues);
            my $issueB = shift(@issues);
            my $issueC = shift(@issues);

            print ntime()
              . " Ranking issue [$issueC] above [$issueB] and below [$issueA]...";

            my $output =
`curl -s -X PUT --data '{"issueKeys":["$issueC"],"rankBeforeKey":"$issueB","rankAfterKey":"$issueA","customFieldId":10004}' -H "Content-Type: application/json" http://localhost:8521/rest/greenhopper/1.0/rank`;
            if ( $output =~ m/success/ ) {
                print "success";
            }
            else {
                print "failure";
            }
            print ". Rest calls [$rankingCount]\n";
            $rankingCount++;
        }
    }
}
else {
    my $restartCounter = 0;
    while (1) {
        sleep(10);
        print "\n" . ntime() . " Interface shutdown\n";
        `ifconfig en0 down`;
        sleep(10);
        `ifconfig en0 up`;
        print "\n" . ntime() . " Interface brought up\n";
        sleep(10);
        $restartCounter++;

        if ( $restartCounter eq 30 )    #restart JIRA every 15 minutes
        {
            restartJIRA();
            $restartCounter = 0;
        }
    }
}

sub ntime {
    my ( $sec, $min, $hr, $day, $mon, $year ) = localtime();
    return sprintf(
        "[%02d/%02d/%04d %02d:%02d:%02d]",
        $day, $mon + 1, 1900 + $year,
        $hr, $min, $sec
    );
}

sub restartJIRA {
    print( "\n" . ntime() . " Restarting JIRA\n" );
    `ifconfig en0 up`;
    sleep(5);
    `cd $jirainst/bin; ./shutdown.sh`;
    `killtc`;
    `cd $jirainst/bin; ./startup.sh`;
    print( ntime() . " Waiting for JIRA to startup completely\n" );
    `wget -q http://localhost:8521`;
    sleep(120);#//No other way to easily determine whether JIRA is completely up
}

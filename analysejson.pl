use strict;
use JSON;

my $sacusername     = "rpillai";
my $sacpassword     = "secret";
my $totalIssues     = 0;
my $issuesWithSz    = 0;
my $uniqueCustomers = 0;
my @diffCustomers   = ();

for ( my $day = 0 ; $day < 180 ; $day++ ) {
    my $prevDay = $day + 1 . "d";
    my $currDay = $day . "d";

#`curl -u $sacusername:$sacpassword 'https://support.atlassian.com/rest/api/2/search?jql=project%20%3D%20JSP%20AND%20status%20in%20(Resolved%2C%20Closed)%20AND%20"Number%20of%20attachments"%20>%3D%20"1"%20AND%20created%20>%3D%20-$prevDay%20AND%20created%20<%3D%20-$currDay%20ORDER%20BY%20created%20ASC&fields=attachment&maxResults=1000' > $day.searchrr.json`;
    my $json;
    {
        local $/;    #enable slurp
        open my $fh, "<", "$day.searchrr.json";
        $json = <$fh>;
    }
    my $decoded_json = &decode_json($json);

    my %data = %$decoded_json;

    my $issueCount = 0;
    my %authors;
    foreach ( $data{issues} ) {
        foreach (@$_) {
            $totalIssues++;
            my $issueKey = $_->{key};
            print("$totalIssues. Analysing $issueKey\n");
            foreach ( @{ $_->{fields}->{attachment} } ) {
                my %attachment = %$_;
                my $author     = $attachment{author}->{emailAddress};
                my $filename   = $attachment{filename};
                my $content    = $attachment{content};
                my $id         = $attachment{id};

        #print "author=$author, filename=$filename, content=$content, id=$id\n";

                if ( $filename =~ /JIRA_support/ ) {
                    $issuesWithSz++;
                    if ( !exists( $authors{$author} ) ) {
                        my ( $keypattern, $issuecount, $projectcount ) =
                          &getdata( $id, $content );
                        print(
"Pattern: $author,$issueKey,$keypattern,$issuecount,$projectcount\n"
                        );

                        if ($keypattern) {
                            $authors{$author} = 1;
                            $uniqueCustomers++;
                            if ( $keypattern ne "([A-Z][A-Z]+)" ) {

                                push( @diffCustomers,
"$author,$issueKey,$keypattern,$issuecount,$projectcount"
                                );
                                print(
"Diff customer : $author,$issueKey,$keypattern,$issuecount,$projectcount\n"
                                );
                                last;
                            }

                            #print "$author - skipping\n";
                        }
                    }
                    else {
                        print("Author already checked\n");
                    }
                }
            }
        }
    }
}
print
"totalIssues=$totalIssues, issuesWithSz=$issuesWithSz, uniqueCustomers=$uniqueCustomers\n";

sub getdata {
    my ( $id, $content ) = @_;
    my $datafile = "datadir/$id.data";
    if ( -e $datafile ) {
        open FF, $datafile;
        my $data = <FF>;
        chomp($data);
        close(FF);
        return split( /,/, $data );
    }
    my $keypattern;
    my $issuecount;
    my $projectcount;
    print "Downloading $content...\n";
`wget --quiet -O $id.zip '$content?os_username=$sacusername&os_password=$sacpassword'`;
    if ( !$? ) {
        my @appdata =
          `unzip -p $id.zip application-properties/JIRA-application.properties`;
        @appdata = `unzip -p $id.zip application-properties/application.xml`
          if ($?);
        if ($?) {
            mkdir("tmpdir");
            `unzip $id.zip -d tmpdir/`;
            if ( !$? )    #Issues with unzip, leave this issue
            {
                @appdata = ();
                my @files = `find tmpdir -type f`;
                my @expr  = (
                    'jira.projectkey.pattern',
                    '<Issues>(\d+)<\/Issues>',
                    '<Projects>(\d+)<\/Projects>',
                    'Issues\s+:\s(\d+)',
                    'Projects\s+:\s(\d+)',
                    'Database.Statistics.Issues=',
                    'Database.Statistics.Projects=',
                    'Application.Properties.jira.projectkey.pattern='
                );

                my $expression = "(" . join( ')|(', @expr ) . ")";

                foreach (@files) {
                    chomp;
                    if (m/\.(log)|(out)^/) {

                        #print("[$_]\n");
                        open FF, "$_" or print("failed to open $_\n");
                        foreach (<FF>) {
                            if (m/$expression/) {
                                chomp;
                                push( @appdata, $_ );
                            }
                        }
                    }
                }
            }
            `rm -rf tmpdir`;
        }
        `rm $id.zip`;
        foreach (@appdata) {
            if (m/<Issues>(\d+)<\/Issues>/) {
                $issuecount = $1 if ( !( $issuecount > $1 ) );
            }
            elsif (m/<Projects>(\d+)<\/Projects>/) {
                $projectcount = $1 if ( !( $projectcount > $1 ) );
            }
            elsif (m/<jira.projectkey.pattern>(.*?)<\/jira.projectkey.pattern>/)
            {
                $keypattern = $1;
            }
            elsif (m/Database.Statistics.Issues=(\d+)/) {
                $issuecount = $1 if ( !( $issuecount > $1 ) );
            }
            elsif (m/Issues\s+:\s(\d+)/) {
                $issuecount = $1 if ( !( $issuecount > $1 ) );
            }
            elsif (m/Database.Statistics.Projects=(\d+)/) {
                $projectcount = $1 if ( !( $projectcount > $1 ) );
            }
            elsif (m/Application.Properties.jira.projectkey.pattern=(.*)/) {
                $keypattern = $1;
            }
            elsif (m/jira.projectkey.pattern\s+:\s(.*)/) {
                $keypattern = $1;
            }
            elsif (m/Projects\s+:\s(\d+)/) {
                $projectcount = $1 if ( !( $projectcount > $1 ) );
            }
        }
        $keypattern   = ss($keypattern);
        $projectcount = ss($projectcount);
        $issuecount   = ss($issuecount);
    }
    else {
        print "Failed to download $content\n";
    }
    if ($keypattern) {
        mkdir "datadir" if ( !-d "datadir" );
        open FF, ">$datafile";
        print FF "$keypattern,$issuecount,$projectcount";
        close FF;
    }
    else {
        print("Some issue in getting the data from attachment\n");
    }
    return ( $keypattern, $issuecount, $projectcount );
}

sub ss {
    my $string = shift;
    $string =~ s/\r//g;
    $string =~ s/\n//g;
    return $string;
}
